$(document).on('ready',function() {
    console.log( "ready!" );
    //translate home
    $(".breadcrumb" ).find('.first').find('a').text('Главная');
    //remove user mini picture from profile page
    $(".profile" ).find('.user-picture').addClass("hidden");
    // show modal window
    $('#myModal').on('shown.bs.modal', function () {
 	 $('#myInput').focus()
	});
	//
	 $('.text-center').removeClass("text-center");
	 $('.views-table.cols-0.table').removeClass('views-table').removeClass('cols-0').removeClass('table');
	 $('p:contains("There is currently no content classified with this term")').html('');

     $('.node.node-article.node-promoted').html('');


    $('ul.menu li').each(function(i,elem) {
        var $a = $(this).children();
        if ($a.hasClass('dropdown-toggle')){
            $b = $(elem).text().split(' ', 1);
           
            $a.attr("data-text", $b );
        } else {
        $a.attr("data-text", $(elem).text() );
        }
    });


});

