<?php


/**
 * implements hook breadcrombs
 */

 function custom_bootstrap_subtheme_breadcrumb($variables) {
  $output = '';
  $breadcrumb = $variables['breadcrumb'];

  // Determine if we are to display the breadcrumb.
  $bootstrap_breadcrumb = theme_get_setting('bootstrap_breadcrumb');
  if (($bootstrap_breadcrumb == 1 || ($bootstrap_breadcrumb == 2 && arg(0) == 'admin')) && !empty($breadcrumb)) {
    $output = theme('item_list', array(
      'attributes' => array(
        'class' => array('breadcrumb'),
      ),
      'items' => $breadcrumb,
      'type' => 'ol',
    ));
    return $output;
  }
}


/**
 * implements hook  preprocess region (adding class row to <front> region-content)
 */
function custom_bootstrap_subtheme_preprocess_region(&$variables, $hook) {
    if($variables['region'] == "content" && $variables['title']="Главная"){
        // $variables['classes_array'][] = 'row';
    }
}
